#!/usr/bin/python3
import os
import sys

sys.path.insert(0, '/usr/lib/python3/dist-packages/')

from sherlock.sherlock import main

if __name__ == "__main__":
    main()
